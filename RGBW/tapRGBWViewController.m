//
//  tapRGBWViewController.m
//  RGBW
//
//  Created by Li Xu on 7/29/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "tapRGBWViewController.h"

const int labelBPCTag = 2000;
const float gameSetDuration = 5.0; //30.0; //5.0; // seconds
const float delayTolerance = 400.0; // minisecond

@interface tapRGBWViewController ()

@end

@implementation tapRGBWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self tapRGBWInit];
}

- (void)saveSettingPreference
{
    // Store the data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
//    [defaults setObject:BgThemeID forKey:@"BackgroundThemeID"];
//    
//    [defaults setBool:KeepScreenOn forKey:@"KeepScreenOnID"];
//    [defaults setBool:KeepAdOn forKey:@"KeepAdOnID"];
    
    [defaults setInteger:userPlayLevel forKey:@"userPlayLevel"];
    [defaults setInteger:userReachedLevel forKey:@"userReachedLevel"];
    [defaults setInteger:userReachedScore forKey:@"userReachedScore"];
    
    [defaults synchronize];
    
    
    //NSLog(@"set selectedNoteCode = %li", (long)[defaults integerForKey:@"selectedNoteCode"]);
    // NSLog(@"Data saved");
}


-(void) getSettingPreference
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
//    BgThemeID = [defaults stringForKey:@"BackgroundThemeID"];
//    if(BgThemeID == nil)    {        BgThemeID = @"Chocolate";   }
//    NSLog(@"BackgroundThemeID = %@", BgThemeID);
//    mainViewBackgroundTheme = [[GlobalPara sharedGlobalPara] getBackgroundThemeColorwithThemeID:BgThemeID];
//
//    
//    KeepScreenOn = ([defaults objectForKey:@"KeepScreenOnID"] == nil) ? YES : [defaults boolForKey:@"KeepScreenOnID"];
//    KeepAdOn = ([defaults objectForKey:@"KeepAdOnID"] == nil) ? NO : [defaults boolForKey:@"KeepAdOnID"];
//    
//    printf("KeepScreenOn = %d \n", KeepScreenOn? 1:0);
//    printf("KeepAdOn = %d \n", KeepAdOn? 1:0);

    
    // NSLog(@"get userPlayLevel = %li", (long)[defaults integerForKey:@"userPlayLevel"]);
    userPlayLevel = ([defaults objectForKey:@"userPlayLevel"] == nil) ? 1 : (int)[defaults integerForKey:@"userPlayLevel"];
    userReachedLevel = ([defaults objectForKey:@"userReachedLevel"] == nil) ? 1 : (int)[defaults integerForKey:@"userReachedLevel"];
    userReachedScore = ([defaults objectForKey:@"userReachedScore"] == nil) ? 0 : (int)[defaults integerForKey:@"userReachedScore"];
    
    printf("userPlayLevel = %d \n", userPlayLevel);
    printf("userReachedLevel = %d \n", userReachedLevel);
    printf("userReachedScore = %d \n", userReachedScore);


}


- (void) tapRGBWInit
{
    [UIApplication sharedApplication].idleTimerDisabled = YES; // KeepScreenOn? YES : NO;
    deviceAbsWidth = self.view.frame.size.width;
    deviceAbsHeight = self.view.frame.size.height;
    if(deviceAbsWidth > deviceAbsHeight)
    {
        float tempwh = deviceAbsWidth;
        deviceAbsWidth = deviceAbsHeight;
        deviceAbsHeight = tempwh;
    }
//    iDeviceModel = [[GlobalPara sharedGlobalPara] getiDeviceModelwithWidth: deviceAbsWidth withHeight: deviceAbsHeight];

    
    float vwidth = deviceAbsWidth;
    float vheight = deviceAbsHeight;
    
    //configure iRate
    //[iRate sharedInstance].appStoreID = kAppStoreID; // Replace this
    //[iRate sharedInstance].previewMode = YES;
    //[iRate sharedInstance].promptForNewVersionIfUserRated = YES;
    
    //    char a[] = "RGBW";
    //    [self permute: a startix:0 endix: 3];
    RGBWPermutation = [[NSArray alloc] initWithObjects:@"RGBW",@"BGWR",@"GWRB",@"RWBG",@"BRGW",
                       @"GRWB",@"RGWB",@"WGBR",@"GBRW",@"RBWG",
                       @"WRGB",@"GRBW",@"BGRW",@"RGWB",@"GWBR",
                       @"BWRG",@"RBGW",@"GBWR",@"RBGW",@"GBWR",
                       @"BWRG",@"RWGB",@"GRBW",@"BRWG",nil];
//    arrayBPMMapLevel  = @[@10.0,@20.0, @30.0, @40.0, @50.0, @52.0, @54.0, @56.0, @58.0, @60.0,
//                          @61.0, @62.0, @63.0, @64.0, @65.0, @66.0, @67.0, @68.0, @69.0, @70.0];
    float tempaa[] = {10.0, 20.0, 30.0, 40.0, 50.0, 52.0, 54.0, 56.0, 58.0};
    arrayBPMMapLevel = [[NSMutableArray alloc] init];
    for(int i = 0; i<9; i++)
    {
        [arrayBPMMapLevel addObject:[NSNumber numberWithFloat:tempaa[i]]];
    }
    for(int i = 60; i<=150; i+=1)
    {
        [arrayBPMMapLevel addObject:[NSNumber numberWithFloat:i]];
    }
    
    [self getSettingPreference];
    beatsPerMinute = [self getMappedBPM:userPlayLevel];
    
    NSString *soundPath =[[NSBundle mainBundle] pathForResource:@"Classic_LeadingBeat" ofType:@"caf"];
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    LeadingBeatAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:nil];
    
    soundPath =[[NSBundle mainBundle] pathForResource:@"Digital3_LeadingBeat" ofType:@"caf"];
    soundURL = [NSURL fileURLWithPath:soundPath];
    MatchBeatAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:nil];
    
    float tempy = 0.07 * vheight; //0.045 * vheight;
    progressView = [[UIProgressView alloc] init];
    progressView.frame = CGRectMake(0.1*vwidth,tempy,0.8*vwidth,0.05 * vheight);;
    [progressView setProgress:0.0 animated:YES];
    [self.view addSubview:progressView];
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 6.0f);
    progressView.transform = transform;
    //progressView.hidden = YES;
    
    tempy += 0.0*vheight;
    labelLevelScore = [[UILabel alloc] initWithFrame:CGRectMake(0.0*vwidth,tempy,1.0*vwidth,0.1 * vheight)];
    labelLevelScore.backgroundColor=[UIColor clearColor];
    //labelLevelScore.textColor=[UIColor blueColor];
    labelLevelScore.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];
    [labelLevelScore setTextAlignment:NSTextAlignmentCenter];
    [labelLevelScore setText:[NSString stringWithFormat:@"Score: 0"]];
    [self.view addSubview:labelLevelScore];
    
    tempy += 0.09 * vheight; //0.045 * vheight;
    labelArrayBeatPosy = tempy;
    labelArrayBeatWidth = vwidth;
    labelArrayBeatHeight = 0.45*vheight;
    [self addButtonArrayLED];
    [self resetButtonArrayLED];

    labelColorText = [[UILabel alloc] initWithFrame:CGRectMake(0.35*vwidth,tempy+0.5*(labelArrayBeatHeight - 0.1*vheight),0.3*vwidth,0.1 * vheight)];
    labelColorText.backgroundColor=[UIColor blueColor];
    labelColorText.textColor=[UIColor greenColor];
    labelColorText.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
    [labelColorText setTextAlignment:NSTextAlignmentCenter];
    [labelColorText setText:[NSString stringWithFormat:@"BLACK"]];
    [[labelColorText layer] setBorderWidth:1.0f];
    [[labelColorText layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:labelColorText];
    
    tempy += labelArrayBeatHeight; //0.045 * vheight;
    labelLevelNum = [[UILabel alloc] initWithFrame:CGRectMake(0.0*vwidth,tempy,1.0*vwidth,0.1 * vheight)];
    labelLevelNum.backgroundColor=[UIColor clearColor];
    //labelLevelNum.textColor=[UIColor blueColor];
    labelLevelNum.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];
    [labelLevelNum setTextAlignment:NSTextAlignmentCenter];
    [labelLevelNum setText:[NSString stringWithFormat:@"Level: %d:%d", userPlayLevel,(int)beatsPerMinute]];
    [self.view addSubview:labelLevelNum];
    
    tempy += 0.07 * vheight;
    CGRect frame = CGRectMake(0.1*vwidth,tempy,0.8*vwidth, 0.08*vheight);
    levelSlider = [[UISlider alloc] initWithFrame:frame];
    [levelSlider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [levelSlider setBackgroundColor:[UIColor clearColor]];
    levelSlider.minimumValue = 1;
    levelSlider.maximumValue = userReachedLevel;
    //levelSlider.maximumValue = arrayBPMMapLevel.count - 1;
    levelSlider.continuous = YES;
    levelSlider.value = userPlayLevel;
    [self.view addSubview:levelSlider];
    
    tempy += 0.1 * vheight;
    buttonStart = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonStart addTarget:self
                      action:@selector(btnStartTouchDown:)
            forControlEvents:UIControlEventTouchDown];
    buttonStart.layer.cornerRadius = 20; // 20;
    buttonStart.backgroundColor = [UIColor blackColor];
    [buttonStart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buttonStart setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [buttonStart setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
    [buttonStart.layer setBorderWidth:3.0];
    [buttonStart.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [buttonStart.layer setShadowOffset:CGSizeMake(5, 5)];
    [buttonStart.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [buttonStart.layer setShadowOpacity:0.5];
    [buttonStart setTitle:@"Start" forState:UIControlStateNormal];
    //NSLog(@"%f",self.view.frame.size.height);
    buttonStart.frame = CGRectMake(0.3*vwidth,tempy,0.4*vwidth, 0.08*vheight);
    //[buttonTipEasy.titleLabel setFont:[UIFont systemFontOfSize:(b5inch ? 30: 10)]];
    buttonStart.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:30];
    [self.view addSubview:buttonStart];
    bStart = false;
    

    
    
    
//    adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
//    CGRect adFrame = adView.frame;
//    adFrame.origin.y = 2.5*deviceAbsHeight; // adFrame.origin.y = deviceAbsHeight - adView.frame.size.height;
//    adView.frame = adFrame;
//    [self.view addSubview:adView];
    
    CGPoint origin = CGPointMake(0.0,2.5*deviceAbsHeight);
    adBanner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait origin:origin];
    adBanner.adUnitID = kSampleAdUnitID;
    //    adBanner.delegate = self;
    adBanner.rootViewController = self;
    [self.view addSubview:adBanner];
    [adBanner loadRequest:[GADRequest request]];
    
    CGRect adFrame = adBanner.frame;
    adFrame.origin.y = deviceAbsHeight-adBanner.frame.size.height; // KeepAdOn ?  deviceAbsHeight-adBanner.frame.size.height : 2.5*deviceAbsHeight ;
    adBanner.frame = adFrame;
    
    float widratio = deviceAbsWidth * 1.0 / 320.0;
    popupWindowSize = (0.9 - (widratio-1.0) * 0.08)*vwidth;
}


- (float)getMappedBPM:(int) lev
{
    float bpm;
    
    if(lev >= arrayBPMMapLevel.count - 1)
    {
        bpm = [[arrayBPMMapLevel objectAtIndex:arrayBPMMapLevel.count - 1] floatValue];
    }else
    {
        bpm = [[arrayBPMMapLevel objectAtIndex:lev] floatValue];
    }
    
    return bpm;
}

-(void)sliderAction:(UISlider *)sender
{
    userPlayLevel  = (int)sender.value;
    beatsPerMinute = [self getMappedBPM:userPlayLevel]; //[[arrayBPMMapLevel objectAtIndex:userPlayLevel] floatValue];
    [labelLevelNum setText:[NSString stringWithFormat:@"Level: %d:%d", userPlayLevel,(int)beatsPerMinute]];
    //-- Do further actions
}

- (void)refreshPracticeTimer
{
    if(bStart)
    {
        beatStartTime = [NSDate date];
        lastAlreadyTapped = alreadyTapped;
        alreadyTapped= NO;
        lastRGBWRandix = RGBWRandix;
        lastColorTextRandix = ColorTextRandix;
        [self resetButtonArrayLEDBackground];
        [self resetColorText];
        [LeadingBeatAudio play];
        float progressratio = tapCounter * 1.0 / (gameSetDuration / (60.0/beatsPerMinute));
        [progressView setProgress:progressratio animated:YES];
    
        tapCounter++;
    }
}



- (IBAction)btnStartTouchDown:(id)sender {
    printf("--------------------------\n");
    
    if(bStart == false)// && bMetroThreadRunning == false)
    {
        bStart = true;
        buttonStart.enabled = NO;
        levelSlider.enabled = NO;
        tapCounter = 0;
        levelScore = 0;
        [labelLevelScore setText:[NSString stringWithFormat:@"Score: %d",levelScore]];
        
        [self refreshPracticeTimer];
        
        if (practiceTimer) [practiceTimer invalidate];
        practiceTimer = [NSTimer
                         scheduledTimerWithTimeInterval: (60.0/beatsPerMinute)
                         target:self
                         selector:@selector(refreshPracticeTimer)
                         userInfo:nil
                         repeats:YES
                         ];
        
        
        //[buttonStart setTitle:@"Stop" forState:UIControlStateNormal];
//        [buttonStart setBackgroundImage:[UIImage imageNamed:@"TempoEasy_Stop.png"] forState:UIControlStateNormal];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, gameSetDuration * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //this will be executed after 2 seconds
            bStart = false;
            buttonStart.enabled = YES;
            levelSlider.enabled = YES;
            if (practiceTimer) [practiceTimer invalidate];
            [progressView setProgress:0.0 animated:YES];
            [self showMessageAfterLevel];
        });
    }
    else if(bStart == true)// && bMetroThreadRunning == true)
    {
        bStart = false;
        //[buttonStart setTitle:@"Start" forState:UIControlStateNormal];
        
        if (practiceTimer) [practiceTimer invalidate];
    }
}

- (void)cancelButtonClicked:(MJSecondDetailViewController *)aSecondDetailViewController
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    //printf("selected and exit \n");
    printf("cancelButtonClicked \n");
    
    levelSlider.maximumValue = userReachedLevel;
    levelSlider.value = userPlayLevel;
    beatsPerMinute = [self getMappedBPM:userPlayLevel];
    [labelLevelNum setText:[NSString stringWithFormat:@"Level: %d:%d", userPlayLevel,(int)beatsPerMinute]];
    
    [labelLevelScore setText:[NSString stringWithFormat:@"Score: %d",levelScore]];
}

- (void)showMessageAfterLevel {
    if(secondDetailViewController == nil) {
        secondDetailViewController = [[MJSecondDetailViewController alloc] initWithNibName:@"MJSecondDetailViewController" bundle:nil];
    }
    // no control on the position of popup
    secondDetailViewController.view.frame = CGRectMake(0, 0, popupWindowSize, popupWindowSize * 9 /12 );
    secondDetailViewController.bgColor = [UIColor brownColor];
    
    int finishRatio = (int)(100.0*levelScore / (gameSetDuration/60.0*beatsPerMinute));
    NSString* tempstr = @"";
    if(finishRatio >=100)
    {
        finishRatio = 100;
        tempstr = [NSString stringWithFormat:@"Finished: %d%%\nPerfect!",finishRatio];
    }else     if(finishRatio >=90)
    {
        tempstr = [NSString stringWithFormat:@"Finished: %d%%\nExcellent!",finishRatio];
    }else    if(finishRatio >=80)
    {
        tempstr = [NSString stringWithFormat:@"Finished: %d%%\nGood job!",finishRatio];
    }else
    {
        tempstr = [NSString stringWithFormat:@"Finished: %d%%\nTry again",finishRatio];
    }
    
    secondDetailViewController.statusMessage = tempstr;
    
    if(userPlayLevel == userReachedLevel && userReachedScore < levelScore)
    {
        userReachedScore = levelScore;
    }
    
    if(finishRatio >=80)
    {
        if(userPlayLevel == userReachedLevel)
        {
            userReachedLevel++;
            userReachedScore = 0;
            
            secondDetailViewController.statusMessage = [NSString stringWithFormat:@"%@\n\nLevel  %d  opened", tempstr, userReachedLevel];;
        }
        
        userPlayLevel++;
    }
    levelScore = 0;
    
    [self saveSettingPreference];
    
    [secondDetailViewController popupVCSetMessage];
    secondDetailViewController.delegate = self;
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
}

-(void)addButtonArrayLED
{
    int rowsize = 2; //beatSubDivisionMax;
    int colsize = 2; //beatsPerCycleMax;
    for( int i = 0; i < colsize; i++ ) {
        for( int j = 0; j < rowsize; j++ ) {
            buttonArrayLED = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [buttonArrayLED setTag: i*rowsize + j + labelBPCTag];
            [buttonArrayLED addTarget:self action:@selector(buttonArrayLEDClicked:) forControlEvents:UIControlEventTouchDown];
            [[buttonArrayLED layer] setBorderWidth:1.0f];
            [[buttonArrayLED layer] setBorderColor:[UIColor lightGrayColor].CGColor];
            [self.view addSubview:buttonArrayLED];
            buttonArrayLED.enabled = YES;
        }
    }
    //printf("get here 1\n");
}

- (void)buttonArrayLEDClicked:(UIButton*)button
{
    if(bStart == false)
        return;
    
    if(alreadyTapped)
    {
        printf("alreadyTapped $$$$$$$$$$$$ \n");
        return;
    }
    
    int btnix = (int)[button tag] - labelBPCTag;
    
    
    if(lastAlreadyTapped)
    {
        alreadyTapped = YES;
        BOOL matched = [self matchedWithRGBWRandix:RGBWRandix ColorTextRandix:ColorTextRandix Buttonix: btnix];
        if(matched)
        {
            printf("Matched >>>>>>>>> \n");
            levelScore = levelScore + 1;
            [labelLevelScore setText:[NSString stringWithFormat:@"Score: %d",levelScore]];
            [MatchBeatAudio play];
        }
        return;
    }
    
//    printf("lastAlreadyTapped = %d,  alreadyTapped = %d\n", lastAlreadyTapped, alreadyTapped);
//    printf("lastRGBWRandix = %d,  lastColorTextRandix = %d, btnix = %d\n", lastRGBWRandix, lastColorTextRandix, btnix);
    // if within tolerance
    NSTimeInterval timeInterval = [beatStartTime timeIntervalSinceNow];
    printf("timeInterval = %.3f,  delayTolerance = %.3f\n", (float)(-timeInterval) * 1000, delayTolerance);
    if((float)(-timeInterval) * 1000 < delayTolerance) // mini-seconds
    {
        lastAlreadyTapped = YES;
        BOOL lastMatched = [self matchedWithRGBWRandix:lastRGBWRandix ColorTextRandix:lastColorTextRandix Buttonix: btnix];
        if(lastMatched)
        {
            printf("lastMatched ...... \n");
            //alreadyTapped = NO;
            levelScore = levelScore + 1;
            [labelLevelScore setText:[NSString stringWithFormat:@"Score: %d",levelScore]];
            [MatchBeatAudio play];
            return;
        }
    }
    
    alreadyTapped = YES;
    BOOL matched = [self matchedWithRGBWRandix:RGBWRandix ColorTextRandix:ColorTextRandix Buttonix: btnix];
    if(matched)
    {
        printf("Matched ********* \n");
        levelScore = levelScore + 1;
        [labelLevelScore setText:[NSString stringWithFormat:@"Score: %d",levelScore]];
        [MatchBeatAudio play];
    }
    
}

-(BOOL)matchedWithRGBWRandix:(int) rix ColorTextRandix:(int) ctrix Buttonix: (int) btnix
{
    //ColorTextRandix // 0, 1, 2, 3
    NSString* pattern = [RGBWPermutation objectAtIndex:rix];
    NSString * newString = [pattern substringWithRange:NSMakeRange(btnix, 1)];
    //    - (unichar)characterAtIndex:(NSUInteger)index;
    if(([newString  isEqual: @"R"] && ctrix ==0)
       || ([newString  isEqual: @"G"] && ctrix ==1)
       || ([newString  isEqual: @"B"] && ctrix ==2)
       || ([newString  isEqual: @"W"] && ctrix ==3))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


-(void)resetButtonArrayLED
{
    float tempy = labelArrayBeatPosy; /// 0.0* vheight;
    float vwidth = labelArrayBeatWidth;
    float vheight = labelArrayBeatHeight;
    
    int rowsize = 2;
    int colsize = 2;
    float gridwidth = vwidth* 1.0 / colsize;
    float gridheight = 1.0 * vheight / rowsize;
    float topleftx =  0.5*vwidth - colsize / 2.0 * gridwidth;
    
    for( int i = 0; i < 2; i++ ) {
        for( int j = 0; j < 2; j++ ) {
            
            UIButton *label = (UIButton*)[self.view viewWithTag: i*2 + j + labelBPCTag];
            label.frame = CGRectMake(topleftx + i*gridwidth,tempy + j* gridheight,gridwidth,gridheight);
        }
    }
    
    [self resetButtonArrayLEDBackground];
}


-(void)resetButtonArrayLEDBackground
{
//    int rowsize = 2; //beatSubDivisionMax;
//    int colsize = 2; //beatsPerCycleMax;
    RGBWRandix = arc4random() % 12; //(rowsize*colsize);
    
    NSString* pattern = [RGBWPermutation objectAtIndex:RGBWRandix];
    for(int i=0;i<4;i++)
    {
        UIButton *label = (UIButton*)[self.view viewWithTag: i + labelBPCTag];
        NSString * newString = [pattern substringWithRange:NSMakeRange(i, 1)];
        //    - (unichar)characterAtIndex:(NSUInteger)index;
        if([newString  isEqual: @"R"])
        {
            label.backgroundColor= [UIColor redColor];
        }else if([newString  isEqual: @"G"])
        {
            label.backgroundColor= [UIColor greenColor];
        }
        else if([newString  isEqual: @"B"])
        {
            label.backgroundColor= [UIColor blueColor];
        }else if([newString  isEqual: @"W"])
        {
            label.backgroundColor= [UIColor blackColor];
        }
    }
}

-(void)resetColorText
{
    //    int rowsize = 2; //beatSubDivisionMax;
    //    int colsize = 2; //beatsPerCycleMax;
    ColorTextRandix = arc4random() % 4; //(rowsize*colsize);
    if(ColorTextRandix == 0)
    {
        [labelColorText setText:[NSString stringWithFormat:@"RED"]];
    }else if(ColorTextRandix == 1)
    {
        [labelColorText setText:[NSString stringWithFormat:@"GREEN"]];
    }
    else if(ColorTextRandix == 2)
    {
        [labelColorText setText:[NSString stringWithFormat:@"BLUE"]];
    }else if(ColorTextRandix == 3)
    {
        [labelColorText setText:[NSString stringWithFormat:@"BLACK"]];
    }
    
    int tempix = arc4random() % 4; //(rowsize*colsize);
    labelColorText.textColor = [self mapColorFromInt:tempix];
    int tempaa[3] = {0,0,0};
    int k = 0;
    for(int i = 0;i<4;i++)
    {
        if (i != tempix)
        {
            tempaa[k] = i;
            k++;
        }
    }
    int tempix2 = arc4random() % 3; //(rowsize*colsize);
    labelColorText.backgroundColor=[self mapColorFromInt:tempaa[tempix2]];
}

-(UIColor *) mapColorFromInt: (int) tempix
{
    UIColor* tempColor;
    if(tempix == 0)
    {
        tempColor = [UIColor redColor];
    }else if(tempix == 1)
    {
        tempColor = [UIColor greenColor];
    }
    else if(tempix == 2)
    {
        tempColor = [UIColor blueColor];
    }else if(tempix == 3)
    {
        tempColor = [UIColor blackColor];
    }
    
    return tempColor;
}

#pragma mark background notifications
- (void)registerForBackgroundNotifications
{
    printf(" registerForBackgroundNotifications .......................... \n");
//	[[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(resignActive)
//												 name:UIApplicationWillResignActiveNotification
//											   object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(DidBecomeActive)
//												 name:UIApplicationDidBecomeActiveNotification
//											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(enterForeground)
												 name:UIApplicationWillEnterForegroundNotification
											   object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(DidEnterBackground)
												 name:UIApplicationDidEnterBackgroundNotification
											   object:nil];
}


- (void)enterForeground
{
    printf("enterForeground .......................... \n");
    [self getSettingPreference];
}

- (void)DidEnterBackground
{
    printf("DidEnterBackground .......................... \n");
    [self saveSettingPreference];
}

- (void)awakeFromNib {
    printf("RGBW awakeFromNib ............. \n");
    
    [self registerForBackgroundNotifications];
}


-(void) swapx: (char *)x andy: (char *)y
{
    char temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

/* Function to print permutations of string
 This function takes three parameters:
 1. String
 2. Starting index of the string
 3. Ending index of the string. */
-(void) permute:(char *) a startix: (int) i endix: (int) n
{
    int j;
    if (i == n)
        printf("%s\n", a);
    else
    {
        for (j = i; j <= n; j++)
        {
            [self swapx: (a+i) andy: (a+j)];
            [self permute: a startix: (i+1) endix: n];
            [self swapx: (a) andy: (a+j)];
            //swap((a+i), (a+j));
            //permute(a, i+1, n);
            //swap((a+i), (a+j)); //backtrack
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
