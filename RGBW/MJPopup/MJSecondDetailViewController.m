//
//  MJSecondDetailViewController.m
//  MJPopupViewControllerDemo
//
//  Created by Martin Juhasz on 24.06.12.
//  Copyright (c) 2012 martinjuhasz.de. All rights reserved.
//

#import "MJSecondDetailViewController.h"

const int notesTagOffset = 10000;

@interface MJSecondDetailViewController ()
{
//    float vwidth;
//    float vheight;
    UIButton* btnClosePopup;
    UILabel* labelMessageText;
}

@end

@implementation MJSecondDetailViewController

@synthesize delegate;

- (void)viewDidLoad
{
    printf("popup  viewDidLoad\n");
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self popupVCInit];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)closePopup:(id)sender
//{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
//        [self.delegate cancelButtonClicked:self];
//    }
//}

- (void)viewWillAppear:(BOOL)animated {
    printf("popup viewWillAppear ... \n");

    
}

- (void) popupVCSetMessage
{
    self.view.backgroundColor = self.bgColor;
   
    float vwidth = self.view.frame.size.width;
    float vheight = self.view.frame.size.height;
    labelMessageText.frame = CGRectMake(0.0*vwidth,0.0*vheight,1.0*vwidth, 0.7*vheight);
    [labelMessageText setTextAlignment:NSTextAlignmentCenter];
    [labelMessageText setText: self.statusMessage];
    
    btnClosePopup.frame = CGRectMake(0.3*vwidth,0.75*vheight, 0.4*vwidth, 0.2*vheight);
}

- (void) popupVCInit
{
    
    float vwidth = self.view.frame.size.width;
    float vheight = self.view.frame.size.height;
    
    self.view.backgroundColor = self.bgColor; //[UIColor colorWithRed:(180/255.0) green:(120/255.0) blue:(90/255.0) alpha:1];//[UIColor brownColor];
    
    labelMessageText = [[UILabel alloc] initWithFrame:CGRectMake(0.0*vwidth,0.0*vheight,1.0*vwidth, 1.0*vheight)];
    labelMessageText.backgroundColor=[UIColor clearColor];
    labelMessageText.textColor=[UIColor blueColor];
    labelMessageText.font = [UIFont fontWithName:@"Helvetica" size:30];
    [labelMessageText setTextAlignment:NSTextAlignmentCenter];
    [labelMessageText setText: self.statusMessage]; //[NSString stringWithFormat:@"Completed\n sdfas\nsdfsadsdsdfsadlfjsdalfjsadfsdja;fsdafsdafsda;fsdasfd."]];
    //labelMessageText.lineBreakMode = UILineBreakModeWordWrap;
    labelMessageText.numberOfLines = 0;
    [self.view addSubview:labelMessageText];
    
    
    btnClosePopup = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnClosePopup addTarget:self
                    action:@selector(btnClosePopupTouchDown:)
          forControlEvents:UIControlEventTouchDown];
    btnClosePopup.layer.cornerRadius = 20; // 20;
    btnClosePopup.backgroundColor = [UIColor blackColor];
    [btnClosePopup setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnClosePopup setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [btnClosePopup setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
//    [buttonStart.layer setBorderWidth:3.0];
//    [buttonStart.layer setBorderColor:[[UIColor blackColor] CGColor]];
//    [buttonStart.layer setShadowOffset:CGSizeMake(5, 5)];
//    [buttonStart.layer setShadowColor:[[UIColor blackColor] CGColor]];
//    [buttonStart.layer setShadowOpacity:0.5];
    [btnClosePopup setTitle:@"OK" forState:UIControlStateNormal];
//    //NSLog(@"%f",self.view.frame.size.height);
    btnClosePopup.frame = CGRectMake(0.0*vwidth,0.0*vheight,1.0*vwidth, 1.0*vheight);
    [btnClosePopup.titleLabel setFont:[UIFont systemFontOfSize: 20]];
    btnClosePopup.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:30];
    [self.view addSubview:btnClosePopup];
    
//    self.view.backgroundColor = self.bgColor; //[UIColor colorWithRed:(180/255.0) green:(120/255.0) blue:(90/255.0) alpha:1];//[UIColor brownColor];
//    
//    float tempy = 0.0* vheight;
//    int rowsize = 2;
//    int colsize = 4;
//    //int gridwidth = (vwidth - 0.0625*vwidth) * 1.0 / colsize;
//    float gridwidth = vwidth* 1.0 / colsize;
//    float gridheight = 1.0 * vheight / rowsize;
//    float topleftx =  0.5*vwidth - colsize / 2.0 * gridwidth;
//    
//    //NSArray *actions;
//    //int notesCodeMap[8] = {1000, 1020, 122, 102, 1222, 1002, 1022, 1220};
//    for( int i = 1; i <= rowsize; i++ ) {
//        for( int j = 1; j <= colsize; j++ ) {
//            btnArrayMusicNotes = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//            [btnArrayMusicNotes setTag:(self.notesMap[(i-1)*colsize + j-1] + notesTagOffset)];
//            [btnArrayMusicNotes addTarget:self action:@selector(btnArrayMusicNotesClicked:) forControlEvents:UIControlEventTouchDown];
//            
//            NSString *imgName = [NSString stringWithFormat:@"musicNote%d.png",self.notesMap[(i-1)*colsize + j-1]];
//            [btnArrayMusicNotes setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
//            btnArrayMusicNotes.frame = CGRectMake(topleftx + (j-1)*gridwidth,tempy + (i-1) * gridheight,gridwidth,gridheight);
//            [[btnArrayMusicNotes layer] setBorderWidth:1.0f];
//            [[btnArrayMusicNotes layer] setBorderColor:[UIColor lightGrayColor].CGColor];
//            
//            [self.view addSubview:btnArrayMusicNotes];
//        }
//    }
    
    
}

- (void)btnClosePopupTouchDown:(UIButton*)button
{
    NSLog(@"btnClosePopupTouchDown");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
}


    
//- (IBAction)closePopup:(id)sender
//{
//    printf("get here 1: \n");
//    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
//        [self.delegate cancelButtonClicked:self];
//    }
//}


@end
