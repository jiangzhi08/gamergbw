//
//  tapRGBWViewController.h
//  RGBW
//
//  Created by Li Xu on 7/29/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "GADBannerView.h"
#import "GADRequest.h"
//#import "iRate.h"

#import "UIViewController+MJPopupViewController.h"
//#import "MJDetailViewController.h"
#import "MJSecondDetailViewController.h"

@interface tapRGBWViewController : UIViewController<MJSecondPopupDelegate>
{
    int deviceAbsWidth;
    int deviceAbsHeight;
        
    ADBannerView *adView;
    GADBannerView *adBanner;
    
    UIButton*           buttonArrayLED;
    float labelArrayBeatPosy;
    float labelArrayBeatWidth;
    float labelArrayBeatHeight;
    
    UIButton*    buttonStart;
    BOOL bStart;
    
    NSTimer						*practiceTimer;
    int tapCounter;
    float beatsPerMinute;
    int userReachedLevel;
    int userReachedScore;
    int userPlayLevel;
    NSMutableArray *arrayBPMMapLevel; //  = @[@1, @2, @3, @4, @5, @6];
    
    
    NSArray *RGBWPermutation;
    int RGBWRandix;
    int lastRGBWRandix;
    
    UILabel* labelColorText;
    int ColorTextRandix;
    int lastColorTextRandix;
    
    UISlider *levelSlider;
    AVAudioPlayer *LeadingBeatAudio;
    AVAudioPlayer *MatchBeatAudio;
    int levelScore;
    UILabel* labelLevelScore;
    UILabel* labelLevelNum;
    
    BOOL alreadyTapped;
    BOOL lastAlreadyTapped;
    
    NSDate *beatStartTime;// = [NSDate date];
    
    UIProgressView *progressView;
    
    MJSecondDetailViewController *secondDetailViewController;
    float popupWindowSize;
}

@end
